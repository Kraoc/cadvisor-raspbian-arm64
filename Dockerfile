FROM            golang:buster AS builder

ARG             VERSION
ARG             GOPATH

RUN             apt-get update && \
                apt-get install make git bash gcc && \
                git clone --depth 1 --branch "master" https://github.com/google/cadvisor.git /go/src/github.com/google/cadvisor

WORKDIR         /go/src/github.com/google/cadvisor

RUN             go env -w GO111MODULE=auto && \
                make build

RUN             ls -alhR --group-directories-first / |more

# ------------------------------------------
# Copied over from deploy/Dockerfile except that the "zfs" dependency has been removed
# a its not available fro Alpine on ARM

FROM            alpine:latest

ARG             VERSION
ARG             GOPATH

MAINTAINER      "Kraoc <cybridenet@gmail.com>"
LABEL           com.centurylinklabs.watchtower.enable=false

RUN             sed -i 's,https://dl-cdn.alpinelinux.org,http://dl-4.alpinelinux.org,g' /etc/apk/repositories

RUN             apk --no-cache add libc6-compat device-mapper findutils thin-provisioning-tools && \
                echo 'hosts: files mdns4_minimal [NOTFOUND=return] dns mdns4' >> /etc/nsswitch.conf && \
                rm -rf /var/cache/apk/*

# Grab cadvisor from the staging directory.
COPY --from=builder /go/src/github.com/google/cadvisor/_output/cadvisor /usr/bin/cadvisor

EXPOSE          8080

HEALTHCHECK     --interval=30s \
                --timeout=3s \
                CMD wget --quiet --tries=1 --spider http://localhost:8080/healthz || exit 1

ENTRYPOINT      ["/usr/bin/cadvisor", "-logtostderr"]
