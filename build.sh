#!/bin/bash
#
# https://blog.ayjc.net/posts/cadvisor-arm/
# https://github.com/klo2k/cadvisor
# https://www.jeremymorgan.com/tutorials/raspberry-pi/install-go-raspberry-pi/
#

clear
cd "$(dirname "$0")" || exit 1

IMAGE_BASE=zogg/cadvisor
IMAGE_NAME_LATEST=${IMAGE_BASE}:latest

export DOCKER_CLI_EXPERIMENTAL=enabled
docker run --privileged --rm tonistiigi/binfmt --install all

VERSION=master
GOPATH=${GOPATH}

docker buildx build --pull \
    --platform "linux/arm64" \
    --output=type=docker \
    --build-arg TZ=Europe/Paris \
    --build-arg CONCURRENCY=$(nproc) \
    --build-arg VERSION=$VERSION \
    --build-arg GOPATH=$GOPATH \
    -t "${IMAGE_NAME_LATEST}" \
    . 2>&1 | tee build.log

exit 0
